#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <errno.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#define vkGetInstanceProcAddr           alkdsfjaldskfjadlfkfj0
#define vkCreateDebugUtilsMessengerEXT  alkdsfjaldskfjadlfkfj1
#define vkDestroyDebugUtilsMessengerEXT alkdsfjaldskfjadlfkfj2
#include <vulkan/vulkan.h>
#undef vkGetInstanceProcAddr
#undef vkCreateDebugUtilsMessengerEXT
#undef vkDestroyDebugUtilsMessengerEXT

#define DEBUG
#define err(...) do {\
	fprintf(stderr, "error: " __VA_ARGS__);\
	exit(-1);\
} while(0)
#define min(x, y) ((x) < (y) ? (x) : (y))
#define max(x, y) ((x) > (y) ? (x) : (y))
#define arrsz(x) (sizeof(x)/sizeof(x[0]))
// how many frames should be processed concurrently
#define MAX_FRAMES_IN_FLIGHT 2

static VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback (
	VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
	VkDebugUtilsMessageTypeFlagsEXT message_type,
	const VkDebugUtilsMessengerCallbackDataEXT *callback_data,
	void *user_data
) { 
	fprintf(stderr, "validation layer: %s\n", callback_data->pMessage);
	return VK_FALSE; // if VK_TRUE, the validation layer is aborted
}


void init_window ();
void init_vulkan ();
void main_loop ();
void cleanup ();

void create_instance ();
void setup_debug_messenger ();
void create_surface ();
void pick_physical_device ();
void create_logical_device ();
void create_swapchain ();
void create_image_views ();
void create_render_pass ();
void create_graphics_pipeline ();
void create_framebuffers ();
void create_command_pool ();
void create_command_buffers ();
void create_semaphores ();

char *read_file (const char *filename, long *size);
PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr;
PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessengerEXT;
PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT;

SDL_Window *window;
int window_width = 800, window_height = 500;
const char *app_name = "sdl vulkan"; //
const char *device_extensions[] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
const char *validation_layers[] = { "VK_LAYER_LUNARG_standard_validation" }; //
VkInstance vulkan_instance;
VkDebugUtilsMessengerEXT debug_messenger;
VkSurfaceKHR window_surface;
VkPhysicalDevice physical_device = VK_NULL_HANDLE;
VkQueue graphics_queue, present_queue;
VkDevice logical_device;
int graphic_queue_index = -1, present_queue_index = -1; //
VkSwapchainKHR swapchain;
VkImage *swapchain_images;
uint32_t swapimage_count;
VkExtent2D swapimage_extent;
VkFormat swapimage_format;
VkImageView *swapimage_views;
VkRenderPass render_pass;
VkPipelineLayout pipeline_layout;
VkPipeline graphics_pipeline;
VkCommandPool command_pool;
VkCommandBuffer *command_buffers;
uint32_t commandbuffer_count;
VkSemaphore image_available_semaphores[MAX_FRAMES_IN_FLIGHT];
VkSemaphore render_finished_semaphores[MAX_FRAMES_IN_FLIGHT];
VkFence inflight_fences[MAX_FRAMES_IN_FLIGHT];
VkFramebuffer *swapchain_framebuffers;
uint32_t framebuffer_count;

int main (int argc, char **argv) {
	init_window();
	init_vulkan();
	main_loop();
	cleanup();
	return 0;
}

	// create vulkan window
void init_window () {
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
	window = SDL_CreateWindow(app_name, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, SDL_WINDOW_VULKAN | SDL_WINDOW_ALLOW_HIGHDPI);
	if (!window) err("failed to create SDL Vulkan window: %s\n", SDL_GetError());
	printf("sdl created window of size %dx%d successfully!\n", window_width, window_height);
}

	// initialize vulkan
void init_vulkan () {
	create_instance();
	setup_debug_messenger();
	create_surface();
	pick_physical_device();
	create_logical_device();
	create_swapchain();
	create_image_views();
	create_render_pass();
	create_graphics_pipeline();
	create_framebuffers();
	create_command_pool();
	create_command_buffers();
	create_semaphores();
}

void/* vulkan_instance*/ create_instance (/*window, validatin_layers*/) {
	VkApplicationInfo app_info;
	const char **extension_names;
	VkExtensionProperties *extensions;
	VkLayerProperties *available_layers;
	VkInstanceCreateInfo inst_info;
	uint32_t extname_count, extension_count, layer_count;
	SDL_bool fine;
	VkResult result;

	app_info = (VkApplicationInfo) {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pApplicationName = app_name,
		.applicationVersion = VK_MAKE_VERSION(1, 0, 0),
		.pEngineName = "No Engine",
		.engineVersion = VK_MAKE_VERSION(1, 0, 0),
		.apiVersion = VK_API_VERSION_1_0,
	};
	// check for required (if it's not a off-screen rendering program) window system integration (WSI) extensions
	fine = SDL_Vulkan_GetInstanceExtensions(window, &extname_count, NULL);
	if (!fine) err("failed to get instance extensions: %s\n", SDL_GetError());
	extension_names = malloc((extname_count + 1) * sizeof(char *)); // +1 = debug extension
	SDL_Vulkan_GetInstanceExtensions(window, &extname_count, extension_names);
	printf("sdl enumerated %u available extensions:\n", extname_count);
	for (int i = 0; i < extname_count; i++) {
		printf("\t%s\n", extension_names[i]);
	}
#ifdef DEBUG
	extension_names[extname_count++] = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
#endif

	// check for all available instance extentions
	vkEnumerateInstanceExtensionProperties(NULL, &extension_count, NULL);
	extensions = malloc(extension_count * sizeof(VkExtensionProperties));
	vkEnumerateInstanceExtensionProperties(NULL, &extension_count, extensions);
	printf("vulkan enumerated %u available instance extensions:\n", extension_count);
	for (int i = 0; i < extension_count; i++) {
		printf("\t%s\n", extensions[i].extensionName);
	}
	free(extensions);

#ifdef DEBUG
	// check for validation layers
	vkEnumerateInstanceLayerProperties(&layer_count, NULL);
	available_layers = malloc(layer_count * sizeof(VkLayerProperties));
	vkEnumerateInstanceLayerProperties(&layer_count, available_layers);
	printf("vulkan enumerated %u instance layers:\n", layer_count);
	for (int i = 0; i < layer_count; i++) {
		printf("\t%s\n", available_layers[i].layerName);
	}
#endif
	
	// create vulkan instance
	inst_info = (VkInstanceCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pApplicationInfo = &app_info,
		.enabledExtensionCount = extname_count,
		.ppEnabledExtensionNames = extension_names, // all available instances
#ifdef DEBUG
		.enabledLayerCount = arrsz(validation_layers),
		.ppEnabledLayerNames = validation_layers,
	};
	free(available_layers);
#else
	};
#endif

	result = vkCreateInstance(&inst_info, NULL, &vulkan_instance);
	if (result != VK_SUCCESS) err("failed to create instance; VkResult = %d\n", result);
	printf("vulkan instance successfully created!\n");
	free(extension_names);
}


void/* debug_messenger*/ setup_debug_messenger (/*vulkan_instance, debug_callback*/) {
#ifdef DEBUG
	VkDebugUtilsMessengerCreateInfoEXT messenger_info;
	VkResult result;

	// setup validation layer (debug) callbacks
	messenger_info = (VkDebugUtilsMessengerCreateInfoEXT) {
		.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
		.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
		.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
		.pfnUserCallback = debug_callback,
		.pUserData = NULL // optional data sent to callback function
	};
	// retrieve function pointer retriever
	vkGetInstanceProcAddr = (PFN_vkGetInstanceProcAddr) SDL_Vulkan_GetVkGetInstanceProcAddr();
	if (!vkGetInstanceProcAddr) err("sdl failed to retrieve vkGetInstanceProcAddr: %s\n", SDL_GetError());
	printf("vkGetInstanceProcAddr acquired!\n");
	// retrieve debug messenger creator
	vkCreateDebugUtilsMessengerEXT = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(vulkan_instance, "vkCreateDebugUtilsMessengerEXT");
	if (!vkCreateDebugUtilsMessengerEXT) err("failed to get address of vkCreateDebugUtilsMessengerEXT function\n");
	printf("vkCreateDebugUtilsMessengerEXT acquired!\n");
	// create debuger messenger
	result = vkCreateDebugUtilsMessengerEXT(vulkan_instance, &messenger_info, NULL, &debug_messenger);
	if (result != VK_SUCCESS) err("vkCreateDebugUtilsMessengerEXT failed; VkResult = %d\n", result);
	// retrieve debug messenger destroyer
	vkDestroyDebugUtilsMessengerEXT = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(vulkan_instance, "vkDestroyDebugUtilsMessengerEXT");
	if (!vkDestroyDebugUtilsMessengerEXT) err("failed to get address of vkDestroyDebugUtilsMessengerEXT function\n");
	printf("vkDestroyDebugUtilsMessengerEXT acquired!\n");
#endif
}

void /*window_surface*/ create_surface (/*vulkan_instance*/) {
	SDL_bool fine;

	// create vulkan surface
	fine = SDL_Vulkan_CreateSurface(window, vulkan_instance, &window_surface);
	if (!fine) err("sdl failed to create vulkan's window surface: %s\n", SDL_GetError());
	printf("vulkan's window surface created!\n");
}

void /*physical_device*/pick_physical_device (/*vulkan_instance*/) {
	VkPhysicalDevice *devices;
	VkPhysicalDeviceProperties device_properties;
	VkPhysicalDeviceFeatures device_features;
	uint32_t device_count;
	int max_gpu_score = 0, best_gpu_id = -1, gpu_score = 0;

	// search for physical devices that support vulkan
	vkEnumeratePhysicalDevices(vulkan_instance, &device_count, NULL);
	if (device_count == 0) err("there are no physical devices that support Vulkan here\n");
	devices = malloc(device_count * sizeof(VkPhysicalDevice));
	vkEnumeratePhysicalDevices(vulkan_instance, &device_count, devices);
	printf("vulkan enumerated %u available physical devices:\n", device_count);
	for (int i = 0; i < device_count; i++) {
		vkGetPhysicalDeviceProperties(devices[i], &device_properties);
		vkGetPhysicalDeviceFeatures(devices[i], &device_features);
		printf("\t%s: %s GPU, max 2D texture size = %u, %s geometry shader\n", device_properties.deviceName, device_properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU ? "dedicated" : "integrated", device_properties.limits.maxImageDimension2D, device_features.geometryShader ? "supports" : "does not support");
		gpu_score = (device_properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU ? 1000 : 0) + device_properties.limits.maxImageDimension2D;
		if (gpu_score > max_gpu_score) {
			best_gpu_id = i;
			max_gpu_score = gpu_score;
		}
	} // select the best GPU
	if (best_gpu_id < 0) err("no physical device supports Vulkan here\n");
	physical_device = devices[best_gpu_id]; // todo: choose the best GPU instead of the first
	free(devices);
}


void /*logical_device, graphics_queue, present_queue*/create_logical_device (/*physical_device, window_surface, validation_layers, device_extensions*/) {
	VkQueueFamilyProperties *queue_families;
	float queue_priority = 1.0f;
	uint32_t queuefamily_count;
   	VkBool32 present_support;

	VkDeviceQueueCreateInfo queue_info[2];
	VkDeviceCreateInfo device_info;
	VkPhysicalDeviceFeatures device_features;
	VkResult result;

	// retrieve the list of queue families
	vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queuefamily_count, NULL);
	queue_families = malloc(queuefamily_count * sizeof(VkQueueFamilyProperties));
	vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queuefamily_count, queue_families);
	printf("there are %u queue families in the selected GPU:\n", queuefamily_count);

	for (int i = 0; i < queuefamily_count; i++) {
		vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, i, window_surface, &present_support);
		printf("\tqueue family %d has %u queues and supports { graphic: %s, compute: %s, transfer: %s, sparce memory management: %s, protected memory: %s, presentation: %s} operations\n", i, queue_families[i].queueCount,
			queue_families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT ? "true" : "false",
			queue_families[i].queueFlags & VK_QUEUE_COMPUTE_BIT ? "true" : "false",
			queue_families[i].queueFlags & VK_QUEUE_TRANSFER_BIT ? "true" : "false",
			queue_families[i].queueFlags & VK_QUEUE_SPARSE_BINDING_BIT ? "true" : "false",
			queue_families[i].queueFlags & VK_QUEUE_PROTECTED_BIT ? "true" : "false",
			present_support ? "true" : "false"
		);
		// the graphic and presentation queue families could be the same, to improve performance
		if (graphic_queue_index < 0 && queue_families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			graphic_queue_index = i;
		}
		if (present_queue_index < 0 && present_support) {
			present_queue_index = i;
		}
	}
	if (graphic_queue_index < 0) err("GPU does not have queue families that support graphic operations\n");
	if (present_queue_index < 0) err("GPU does not have queue families that support present operations\n");
	printf("queue families inspected\n");

	
	// create a logical device
	queue_info[0] = queue_info[1] = (VkDeviceQueueCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		.queueFamilyIndex = graphic_queue_index,
		.pQueuePriorities = &queue_priority,
	};
	queue_info[0].queueCount = queue_families[graphic_queue_index].queueCount;
	queue_info[1].queueCount = queue_families[present_queue_index].queueCount;
	free(queue_families);
	device_features = (VkPhysicalDeviceFeatures) {}; // no feature required, for now
	device_info = (VkDeviceCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.pQueueCreateInfos = queue_info,
		.queueCreateInfoCount = arrsz(queue_info) - (graphic_queue_index == present_queue_index),
		.pEnabledFeatures = &device_features,
		.enabledExtensionCount = arrsz(device_extensions),
		.ppEnabledExtensionNames = device_extensions,
#ifdef DEBUG
		.enabledLayerCount = arrsz(validation_layers),
		.ppEnabledLayerNames = validation_layers,
#endif
	};
	result = vkCreateDevice(physical_device, &device_info, NULL, &logical_device);
	if (result != VK_SUCCESS) err("failed to create logical device; VkResult = %d\n", result);
	printf("logical device created\n");

	// retrieve queue handles
	vkGetDeviceQueue(logical_device, graphic_queue_index, 0, &graphics_queue); // for only one queue
	printf("graphics queue handle retrieved!\n");
	vkGetDeviceQueue(logical_device, present_queue_index, 0, &present_queue); // for only one queue
	printf("present queue handle retrieved!\n");
}


void /* swapchain, swapchain_images, swapimage_count, swapimage_fextent, swapimage_format*/create_swapchain (/*physical_device, device_extensions, window_surface*/) {
	VkExtensionProperties *extensions;
	VkSurfaceFormatKHR *surface_formats, surface_format;
	VkSurfaceCapabilitiesKHR surface_capabilities;
	VkPresentModeKHR *present_modes, present_mode;
	VkSwapchainCreateInfoKHR swapchain_info;
	uint32_t format_count, extension_count, presentmode_count;
	int required_extensions = arrsz(device_extensions); 
	VkResult result;

	// checking for swap chain extension support
	vkEnumerateDeviceExtensionProperties(physical_device, NULL, &extension_count, NULL);
	extensions = malloc(extension_count * sizeof(VkExtensionProperties));
	vkEnumerateDeviceExtensionProperties(physical_device, NULL, &extension_count, extensions);
	printf("vulkan enumerated %u device extensions:\n", extension_count);
	for (int i = 0; i < extension_count; i++) {
		printf("\t%s\n", extensions[i].extensionName);
	}
	for (int i = 0; i < arrsz(device_extensions); i++) {
		for (int j = 0; j < extension_count; j++) {
			if (strcmp(device_extensions[i], extensions[j].extensionName)) {
				required_extensions--;
				break;
			}
		}
	}
	if (required_extensions > 0) err("physical device does not support required extensions\n");
	printf("physical device extensions met\n");
	free(extensions);

	// querying details of swap chain support
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, window_surface, &surface_capabilities);
	vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, window_surface, &format_count, NULL);
	if (!format_count) err("no surface formats supported by the physical device\n");
	surface_formats = malloc(format_count * sizeof(VkSurfaceFormatKHR));
	vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, window_surface, &format_count, surface_formats);
	vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, window_surface, &presentmode_count, NULL);
	if (!presentmode_count) err("no present modes supported by the physical device\n");
	present_modes = malloc(presentmode_count * sizeof(VkPresentModeKHR));
	vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, window_surface, &presentmode_count, present_modes);
	printf("surface formats and present modes queried\n");

	// swap chain surface format (color depth)
	if (format_count == 1 && surface_formats[0].format == VK_FORMAT_UNDEFINED) {
		surface_format = (VkSurfaceFormatKHR) {
			.format = VK_FORMAT_B8G8R8A8_UNORM,
			.colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR,
		};
		printf("surface format undefined: choosing B8G8R8A8 and colorspace sRGB\n");
	} else {
		int i;
		for (i = 0; i < format_count; i++) {
			if (surface_formats[i].format == VK_FORMAT_B8G8R8A8_UNORM && surface_formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
				surface_format = surface_formats[i];
				printf("prefered surface format B8G8R8A8 and colorspace sRGB found\n");
				break;
			}
		}
		if (i >= format_count) {
			surface_format = surface_formats[0];
			printf("prefered surface format and colorspace not found: choosing the first choice\n");
		}
	}
	free(surface_formats);

	// swap chain presentation mode
	VkPresentModeKHR best_mode = VK_PRESENT_MODE_FIFO_KHR;
	for (int i = 0; i < presentmode_count; i++) {
		if (present_modes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
			best_mode = present_modes[i];
			break;
		} else if (present_modes[i] == VK_PRESENT_MODE_IMMEDIATE_KHR) {
			best_mode = present_modes[i];
		}
	}
	present_mode = best_mode;
	printf("present mode is %s\n", best_mode == VK_PRESENT_MODE_FIFO_KHR ? "FIFO" : (best_mode ==  VK_PRESENT_MODE_MAILBOX_KHR ? "mailbox" : "immediate"));
	free(present_modes);

	// swap chain extent
	if (surface_capabilities.currentExtent.width != ~ (uint32_t) 0) {
		swapimage_extent = surface_capabilities.currentExtent;
	} else {
		swapimage_extent = (VkExtent2D) {
			.width = max(surface_capabilities.minImageExtent.width, min(surface_capabilities.maxImageExtent.width, window_width)),
			.height = max(surface_capabilities.minImageExtent.height, min(surface_capabilities.maxImageExtent.height, window_height)),
		};
	}
	printf("swap chain images extent of size %ux%u\n", swapimage_extent.width, swapimage_extent.height);

	// number of images to have in the swap chain
	swapimage_count = surface_capabilities.minImageCount + 1;
	if (surface_capabilities.maxImageCount > 0 && swapimage_count > surface_capabilities.maxImageCount) {
		swapimage_count = surface_capabilities.maxImageCount;
	}
	printf("number of swapchain images = %u; (min = %u, max = %u)\n", swapimage_count, surface_capabilities.minImageCount, surface_capabilities.maxImageCount);

	// create swapchain
	swapchain_info = (VkSwapchainCreateInfoKHR) {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.surface = window_surface,
		.minImageCount = swapimage_count,
		.imageFormat = surface_format.format,
		.imageColorSpace = surface_format.colorSpace,
		.imageExtent = swapimage_extent,
		.imageArrayLayers = 1, // always 1, unless stereoscopic 3D application
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, // _TRANSFER_DST_ to do preprocessing
		.preTransform = surface_capabilities.currentTransform,
		.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR, // do not blend window with background
		.presentMode = present_mode,
		.clipped = VK_TRUE,
		.oldSwapchain = VK_NULL_HANDLE,
	};
	const uint32_t queue_family_indices[] = { graphic_queue_index, present_queue_index };
	if (present_queue_index != graphic_queue_index) {
		swapchain_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		swapchain_info.queueFamilyIndexCount = 2;
		swapchain_info.pQueueFamilyIndices = queue_family_indices;
	} else {
		swapchain_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	}
	result = vkCreateSwapchainKHR(logical_device, &swapchain_info, NULL, &swapchain);
	if (result != VK_SUCCESS) err("vulkan failed to create swap chain; VkResult = %d\n", result);
	printf("swap chain created!\n");

	// retrieve the swap chain images
	vkGetSwapchainImagesKHR(logical_device, swapchain, &swapimage_count, NULL);
	swapchain_images = malloc(swapimage_count * sizeof(VkImage));
	vkGetSwapchainImagesKHR(logical_device, swapchain, &swapimage_count, swapchain_images); 
	printf("%u swap chain images retrieved\n", swapimage_count);
	swapimage_format = surface_format.format;
}

void /*swapimage_views*/create_image_views (/*swapimage_count, swapchain_images, swapimage_format, logical_device*/) {
	VkImageViewCreateInfo imageview_info;
	VkResult result;

	// create image views
	swapimage_views = malloc(swapimage_count * sizeof(VkImageView));
	for (int i = 0; i < swapimage_count; i++) {
		imageview_info = (VkImageViewCreateInfo) {
			.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			.image = swapchain_images[i],
			.viewType = VK_IMAGE_VIEW_TYPE_2D,
			.format = swapimage_format,
			.components = (VkComponentMapping) {
				.r = VK_COMPONENT_SWIZZLE_IDENTITY,
				.g = VK_COMPONENT_SWIZZLE_IDENTITY,
				.b = VK_COMPONENT_SWIZZLE_IDENTITY,
				.a = VK_COMPONENT_SWIZZLE_IDENTITY,
			},
			.subresourceRange = (VkImageSubresourceRange) {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		};
		result = vkCreateImageView(logical_device, &imageview_info, NULL, &swapimage_views[i]);
		if (result != VK_SUCCESS) err("failed to create an image view for the %d / %u image: VkResult = %d\n", i, swapimage_count, result);
	}
	printf("%u swap chain image views created\n", swapimage_count);
}


// create render passes
void create_render_pass () {
	VkAttachmentDescription color_attachment;
	VkAttachmentReference colorattachment_reference;
	VkSubpassDescription subpass;
	VkSubpassDependency subpass_dependency;
	VkRenderPassCreateInfo renderpass_info;
	VkResult result;

	color_attachment = (VkAttachmentDescription) {
		.format = swapimage_format,
		.samples = VK_SAMPLE_COUNT_1_BIT,
		.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
		.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
		.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
		.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
		.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
	};
	// subpasses and attachment references
	colorattachment_reference = (VkAttachmentReference) {
		.attachment = 0, // layout(location = 0) out vec4 out_color;
		.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
	};
	subpass = (VkSubpassDescription) {
		.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
		.colorAttachmentCount = 1,
		.pColorAttachments = &colorattachment_reference,
	};
	// subpass dependencies
	subpass_dependency = (VkSubpassDependency) {
		.srcSubpass = VK_SUBPASS_EXTERNAL,
		.dstSubpass = 0,
		.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
		.srcAccessMask = 0,
		.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
		.dstAccessMask= VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
	};
	// render pass
	renderpass_info = (VkRenderPassCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
		.attachmentCount = 1,
		.pAttachments = &color_attachment,
		.subpassCount = 1,
		.pSubpasses = &subpass,
		.dependencyCount = 1,
		.pDependencies = &subpass_dependency,
	};
	result = vkCreateRenderPass(logical_device, &renderpass_info, NULL, &render_pass);

	if (result != VK_SUCCESS) err("failed to create render pass: VkResult = %d\n", result);
	printf("render pass created\n");
}


	// create graphics pipeline
void create_graphics_pipeline () {
	char *vsh_filename = "shaders/vert.spv", *fsh_filename = "shaders/frag.spv";
	char *vertshader_code, *fragshader_code;
	long vsh_code_size, fsh_code_size;
	VkShaderModuleCreateInfo shadermodule_info;
	VkShaderModule vertshader_module, fragshader_module;
	VkPipelineShaderStageCreateInfo vsh_stage_info, fsh_stage_info;
	VkPipelineVertexInputStateCreateInfo vertexinput_info;
	VkPipelineInputAssemblyStateCreateInfo inputassembly_info;
	VkViewport viewport;
	VkRect2D scissor;
	VkPipelineViewportStateCreateInfo viewportstate_info;
	VkPipelineRasterizationStateCreateInfo rasterizer_info;
	VkPipelineMultisampleStateCreateInfo multisample_info;
	VkPipelineColorBlendAttachmentState colorblend_attachment;
	VkPipelineColorBlendStateCreateInfo colorblend_info;
	VkPipelineLayoutCreateInfo pipelinelayout_info;
	VkGraphicsPipelineCreateInfo pipeline_info;
	VkResult result;

	// read shader files
	vertshader_code = read_file(vsh_filename, &vsh_code_size);
	if (!vertshader_code) err("failed to read file %s: %s\n", vsh_filename, strerror(errno));
	fragshader_code = read_file(fsh_filename, &fsh_code_size);
	if (!vertshader_code) err("failed to read file %s: %s\n", fsh_filename, strerror(errno));
	printf("read shader files:\n	%s, of size %ld\n\t%s, of size %ld\n", vsh_filename, vsh_code_size, fsh_filename, fsh_code_size);
	// create shader modules (make a function)
	shadermodule_info = (VkShaderModuleCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.codeSize = vsh_code_size,
		.pCode = (uint32_t *)vertshader_code,
	};
	result = vkCreateShaderModule(logical_device, &shadermodule_info, NULL, &vertshader_module);
	if (result != VK_SUCCESS) err("failed to create shader module for %s: VkResult = %d\n", vsh_filename, result);
	free(vertshader_code);
	shadermodule_info = (VkShaderModuleCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.codeSize = fsh_code_size,
		.pCode = (uint32_t *)fragshader_code,
	};
	result = vkCreateShaderModule(logical_device, &shadermodule_info, NULL, &fragshader_module);
	if (result != VK_SUCCESS) err("failed to create shader module for %s: VkResult = %d\n", fsh_filename, result);
	free(fragshader_code);
	printf("created shader modules\n");
	// create shader stage
	vsh_stage_info = (VkPipelineShaderStageCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
		.stage = VK_SHADER_STAGE_VERTEX_BIT,
		.module = vertshader_module,
		.pName = "main",
	};
	fsh_stage_info = (VkPipelineShaderStageCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
		.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
		.module = fragshader_module,
		.pName = "main", // entrypoint: may combine multiples fragsh into a single shader module and use different entry points to differentiate between their behaviors
		.pSpecializationInfo = NULL, // specialize different values in the shader for different uses
	};
	VkPipelineShaderStageCreateInfo shader_stages[] = { vsh_stage_info, fsh_stage_info };

	// fixed functions
	// vertex input: bindings (spacing bewtween data and if per-{vertx, instance}) and attribute descriptions (type of attr passed to vsh, which bindings and offset)
	vertexinput_info = (VkPipelineVertexInputStateCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
		.vertexBindingDescriptionCount = 0,
		.pVertexBindingDescriptions = NULL, // optional
		.vertexAttributeDescriptionCount = 0,
		.pVertexAttributeDescriptions = NULL, // optional
	};
	// input assembly: kind of geometry and primitive restart enabled/disabled
	inputassembly_info = (VkPipelineInputAssemblyStateCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
		.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
		.primitiveRestartEnable = VK_FALSE,
	};
	// viewports and scissors
	viewport = (VkViewport) {
		.x = 0.0f,
		.y = 0.0f,
		.width = (float)swapimage_extent.width,
		.height = (float)swapimage_extent.height,
		.minDepth = 0.0f,
		.maxDepth = 1.0f,
	};
	scissor = (VkRect2D) {
		.offset = { 0, 0 },
		.extent = swapimage_extent,
	};
	viewportstate_info = (VkPipelineViewportStateCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
		.viewportCount = 1,
		.pViewports = &viewport,
		.scissorCount = 1,
		.pScissors = &scissor,
	};
	// rasterizer
	rasterizer_info = (VkPipelineRasterizationStateCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
		.depthClampEnable = VK_FALSE, // discard fragments beyond near/far instead of clamping them
		.rasterizerDiscardEnable = VK_FALSE, // do not disable output to the framebuffer
		.polygonMode = VK_POLYGON_MODE_FILL,
		.lineWidth = 1.0f,
		.cullMode = VK_CULL_MODE_BACK_BIT,
		.frontFace = VK_FRONT_FACE_CLOCKWISE,
		.depthBiasEnable = VK_FALSE, // enable for shadow mapping
		.depthBiasConstantFactor = 0.0f,
		.depthBiasClamp = 0.0f,
		.depthBiasSlopeFactor = 0.0f,
	};
	// multisample (disabled for now)
	multisample_info = (VkPipelineMultisampleStateCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
		.sampleShadingEnable = VK_FALSE,
		.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
		.minSampleShading = 1.0f, // optional V
		.pSampleMask = NULL,
		.alphaToCoverageEnable = VK_FALSE,
		.alphaToOneEnable = VK_FALSE,
	};
	// color blending
	colorblend_attachment = (VkPipelineColorBlendAttachmentState) {
		.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
		.blendEnable = VK_FALSE,
		.srcColorBlendFactor = VK_BLEND_FACTOR_ONE, // optional V
		.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
		.colorBlendOp = VK_BLEND_OP_ADD,
		.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
		.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
		.alphaBlendOp = VK_BLEND_OP_ADD,
	};
	colorblend_info = (VkPipelineColorBlendStateCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
		.logicOpEnable = VK_FALSE,
		.logicOp = VK_LOGIC_OP_COPY, // optional
		.attachmentCount = 1,
		.pAttachments = &colorblend_attachment,
		.blendConstants = { 0.0f, 0.0f, 0.0f, 0.0f }, // optional
	};
	// pipeline layout (uniform and push values referenced by the shader - runtime updated)
	pipelinelayout_info = (VkPipelineLayoutCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		.setLayoutCount = 0,
		.pSetLayouts = NULL, // optional V
		.pushConstantRangeCount = 0,
		.pPushConstantRanges = NULL,
	};
	result = vkCreatePipelineLayout(logical_device, &pipelinelayout_info, NULL, &pipeline_layout);
	if (result != VK_SUCCESS) err("failed to create pipeline layout: VkResult = %d\n", result);
	printf("pipeline layout created\n");

	// finally create the graphics pipeline by combining all structures
	pipeline_info = (VkGraphicsPipelineCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
		// shaders' descriptions
		.stageCount = 2,
		.pStages = shader_stages,
		// structures describing fixed-function stage
		.pVertexInputState = &vertexinput_info,
		.pInputAssemblyState = &inputassembly_info,
		.pViewportState = &viewportstate_info,
		.pRasterizationState = &rasterizer_info,
		.pMultisampleState = &multisample_info,
		.pDepthStencilState = NULL, // optional
		.pColorBlendState = &colorblend_info,
		.pDynamicState = NULL, // optional
		// graphics pipeline
		.layout = pipeline_layout,
		// render pass
		.renderPass = render_pass,
		.subpass = 0,
		// optional: children pipelines (alike) for faster switching
		.basePipelineHandle = VK_NULL_HANDLE,
		.basePipelineIndex = -1,
	};
	result = vkCreateGraphicsPipelines(logical_device, VK_NULL_HANDLE, 1, &pipeline_info, NULL, &graphics_pipeline);
	if (result != VK_SUCCESS) err("failed to create graphics pipeline: VkResult = %d\n", result);
	printf("graphics pipeline created!\n"); 
	// cleanup shader modules
	vkDestroyShaderModule(logical_device, vertshader_module, NULL);
	vkDestroyShaderModule(logical_device, fragshader_module, NULL);
}

	// framebuffers
void create_framebuffers () {
	VkImageView *attachments;
	VkResult result;

	framebuffer_count = swapimage_count;
	swapchain_framebuffers = malloc(framebuffer_count * sizeof(VkFramebuffer));
	for (int i = 0; i < swapimage_count; i++) {
		attachments = &swapimage_views[i];
		VkFramebufferCreateInfo framebuffer_info = {
			.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
			.renderPass = render_pass,
			.attachmentCount = 1,
			.pAttachments = attachments,
			.width = swapimage_extent.width,
			.height = swapimage_extent.height,
			.layers = 1,
		};
		result = vkCreateFramebuffer(logical_device, &framebuffer_info, NULL, &swapchain_framebuffers[i]);
		if (result != VK_SUCCESS) err("failed to create framebuffer %d: VkResult = %d\n", i, result);
	}
	printf("%u framebuffers created!\n", framebuffer_count);
}

	// command buffers
	// command pools
void create_command_pool () {
	VkCommandPoolCreateInfo commandpool_info;
	VkResult result;

	commandpool_info = (VkCommandPoolCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		.queueFamilyIndex = graphic_queue_index,
		.flags = 0, // optional
	};
	result = vkCreateCommandPool(logical_device, &commandpool_info, NULL, &command_pool);
	if (result != VK_SUCCESS) err("failed to create command pool: VkResult = %d\n", result);
	printf("command pool created\n");
}

	// command buffer allocation
void create_command_buffers () {
	VkCommandBufferAllocateInfo cmdbuffer_alloc_info;
	VkCommandBufferBeginInfo cmdbuffer_begin_info;
	VkRenderPassBeginInfo renderpass_begin_info;
	VkClearValue clear_color;
	VkResult result;

	commandbuffer_count = framebuffer_count;
	command_buffers = malloc(commandbuffer_count * sizeof(VkCommandBuffer));
	cmdbuffer_alloc_info = (VkCommandBufferAllocateInfo) {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.commandPool = command_pool,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		.commandBufferCount = commandbuffer_count,
	};
	result = vkAllocateCommandBuffers(logical_device, &cmdbuffer_alloc_info, command_buffers);
	if (result != VK_SUCCESS) err("failed to allocate commands buffer: VkResult = %d\n", result);
	printf("%u command buffers allocated\n", commandbuffer_count);
	// starting command buffer recording
	for (int i = 0; i < commandbuffer_count; i++) {
		cmdbuffer_begin_info = (VkCommandBufferBeginInfo) {
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
			.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
			.pInheritanceInfo = NULL, // optional
		};
		result = vkBeginCommandBuffer(command_buffers[i], &cmdbuffer_begin_info);
		if (result != VK_SUCCESS) err("failed to begin recording command buffer %d: VkResult = %d\n", i, result);
		// starting a render pass
		clear_color = (VkClearValue) { 0.0f, 0.0f, 0.0f, 1.0f };
		renderpass_begin_info = (VkRenderPassBeginInfo) {
			.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
			.renderPass = render_pass,
			.framebuffer = swapchain_framebuffers[i],
			.renderArea = (VkRect2D) {
				.offset = { 0, 0 },
				.extent = swapimage_extent,
			},
			.clearValueCount = 1,
			.pClearValues = &clear_color,
		};
		// record commands in buffer
		vkCmdBeginRenderPass(command_buffers[i], &renderpass_begin_info, VK_SUBPASS_CONTENTS_INLINE);
		vkCmdBindPipeline(command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphics_pipeline);
		vkCmdDraw(command_buffers[i], 3, 1, 0, 0);
		// finish recording
		vkCmdEndRenderPass(command_buffers[i]);
		result = vkEndCommandBuffer(command_buffers[i]);
		if (result != VK_SUCCESS) err("failed to record command buffer %d: VkResult = %d\n", i, result);
	}
	printf("command buffers recorded!\n");
}

void create_semaphores () {
	VkSemaphoreCreateInfo semaphore_info;
	VkFenceCreateInfo fence_info;
	VkResult result;

	// synchronization: create semaphores and fences
	semaphore_info = (VkSemaphoreCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
	};
	fence_info = (VkFenceCreateInfo) {
		.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
		.flags = VK_FENCE_CREATE_SIGNALED_BIT,
	};
	for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		result = vkCreateSemaphore(logical_device, &semaphore_info, NULL, &image_available_semaphores[i]);
		if (result != VK_SUCCESS) err("failed to create 'image available' semaphore %d: VkResult = %d\n", i, result);
		result = vkCreateSemaphore(logical_device, &semaphore_info, NULL, &render_finished_semaphores[i]);
		if (result != VK_SUCCESS) err("failed to create 'render finished' semaphore %d: VkResult = %d\n", i, result);
		result = vkCreateFence(logical_device, &fence_info, NULL, &inflight_fences[i]);
		if (result != VK_SUCCESS) err("failed to create 'in flight' fence %d: VkResult = %d\n", i, result);
	}
	printf("semaphores and fences created\n");
}

void main_loop () {
	SDL_Event event;
	uint32_t image_index, current_frame = 0;
	VkSubmitInfo submit_info;
	VkPipelineStageFlags wait_stage;
	VkPresentInfoKHR present_info;
	VkResult result;

	printf("starting main loop!\n");
	// main loop impersonator
	bool quit = false;
	uint32_t fps = -1, last_t, t;
	last_t = t = SDL_GetTicks();
	while (!quit) {
		t = SDL_GetTicks();
		fps++;
		if (t - last_t >= 1000) {
			printf("fps = %u\n", fps);
			last_t = t;
			fps = 0;
		}
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				quit = true; break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
				case SDLK_q:
					quit = true; break;
				default:
					break;
				}
				break;
			case SDL_MOUSEBUTTONDOWN:
				quit = true; break;
			default:
				break;
			}
		}
		// draw frame
		vkWaitForFences(logical_device, 1, &inflight_fences[current_frame], VK_TRUE, ~((uint64_t)0));
		vkResetFences(logical_device, 1, &inflight_fences[current_frame]);
		vkAcquireNextImageKHR(logical_device, swapchain, ~((uint64_t)0), image_available_semaphores[current_frame], VK_NULL_HANDLE, &image_index);
		// submitting the command buffer
		wait_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		submit_info = (VkSubmitInfo) {
			.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
			.waitSemaphoreCount = 1,
			.pWaitSemaphores = &image_available_semaphores[current_frame],
			.pWaitDstStageMask = &wait_stage,
			.commandBufferCount = 1,
			.pCommandBuffers = &command_buffers[image_index],
			.signalSemaphoreCount = 1,
			.pSignalSemaphores = &render_finished_semaphores[current_frame],
		};
		result = vkQueueSubmit(graphics_queue, 1, &submit_info, inflight_fences[current_frame]);
		if (result != VK_SUCCESS) err("failed to submit draw command buffer: VkResult = %d\n", result);
		// presentation
		present_info = (VkPresentInfoKHR) {
			.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
			.waitSemaphoreCount = 1,
			.pWaitSemaphores =  &render_finished_semaphores[current_frame],
			.swapchainCount = 1,
			.pSwapchains = &swapchain,
			.pImageIndices = &image_index,
			.pResults = NULL, // optional: VkResult for each swapchain
		};
		vkQueuePresentKHR(present_queue, &present_info);
		current_frame = (current_frame + 1) % MAX_FRAMES_IN_FLIGHT;
	}
	vkDeviceWaitIdle(logical_device);
	printf("main loop finalized\n");
}

	// cleanup
void cleanup () {
	for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		vkDestroySemaphore(logical_device, render_finished_semaphores[i], NULL);
		vkDestroySemaphore(logical_device, image_available_semaphores[i], NULL);
		vkDestroyFence(logical_device, inflight_fences[i], NULL);
	}
	free(command_buffers);
	vkDestroyCommandPool(logical_device, command_pool, NULL);
	for (int i = 0; i < framebuffer_count; i++) {
		vkDestroyFramebuffer(logical_device, swapchain_framebuffers[i], NULL);
	}
	free(swapchain_framebuffers);
	vkDestroyPipeline(logical_device, graphics_pipeline, NULL);
	vkDestroyPipelineLayout(logical_device, pipeline_layout, NULL);
	vkDestroyRenderPass(logical_device, render_pass, NULL);
	for (int i = 0; i < swapimage_count; i++) {
		vkDestroyImageView(logical_device, swapimage_views[i], NULL);
	}
	free(swapchain_images);
	vkDestroySwapchainKHR(logical_device, swapchain, NULL);
	vkDestroyDevice(logical_device, NULL);
#ifdef DEBUG
	vkDestroyDebugUtilsMessengerEXT(vulkan_instance, debug_messenger, NULL);
#endif
	vkDestroySurfaceKHR(vulkan_instance, window_surface, NULL);
	vkDestroyInstance(vulkan_instance, NULL);
	SDL_DestroyWindow(window);
	SDL_Quit();
	printf("all cleaned up\nexiting\n");
}

char *read_file (const char *filename, long *sizep) {
	char *buffer = NULL;
	FILE *file;
	int result;

	// open file
	file = fopen(filename, "rb");
	if (!file) goto read_file__fopen_error;
	// get file size
	result = fseek(file, 0L, SEEK_END);
	if (result < 0) goto read_file__fseek_error;
	*sizep = ftell(file);
	if (*sizep <= 0) goto read_file__fseek_error;
	result = fseek(file, 0L, SEEK_SET);
	if (result < 0) goto read_file__fseek_error;
	// read file into buffer
	buffer = malloc(*sizep);
	if (!buffer) goto read_file__malloc_error;
	fread(buffer, 1, *sizep, file);

read_file__malloc_error:
read_file__fseek_error:
	fclose(file);
read_file__fopen_error:
	return buffer;
}
